# Deber 14

05/11/2020

# Instrucciones

- Generar claves aleatoreas y guardarlas en un archivo.
- Usar algun algortimo para realizar el ordenamiento de las claves.
- Se debe realizar un ordenamiento externo.
  - Se asume que todas las claves no caben en memoria.
- Se debe generar un archivo con un `snapshot` de la lista de claves en cada iteracion de ordenamiento.
