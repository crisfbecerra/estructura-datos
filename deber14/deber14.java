import java.io.*;
import java.util.Scanner;
import java.nio.file.*;

class deber14 {
    final static String fileName = "data";
    static int max_value = 20;
    static int data_size = 20;

    public static void main(String[] args) {
        try {
            createFile();
            saveDataToFile();
            File fl = new File(fileName);
            if (fl.exists()) {
                System.out.println("Los archivos temp_quick_sort muestran los pasos seguidos");
                System.out.println("El arreglo original se encuentra en el primer archivo temp_quick_sort");
                System.out.println("Max value: " + max_value);
                sortFromFile(fl);
            } else {
                System.out.println("Error: File [" + fileName + "] not exist");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static void createFile() throws Exception {
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    public static void saveDataToFile() {
        try (PrintWriter print = new PrintWriter(fileName);) {
            for (int i = 0; i < data_size; i++) {
                int data = (int) (Math.random() * max_value) + 1;
                print.println(data);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void sortFromFile(File file) throws Exception {
        if (!file.exists()) {
            throw new FileNotFoundException("File not found: " + file.getName());
        }
        int size = 0;
        Scanner sc = new Scanner(file);
        // Conteo de datos en el archivo
        while (sc.hasNextLine()) {
            size++;
            sc.nextLine();
        }
        sc.close();
        System.out.println("Data size: " + (size));
        quickSort(file, 0, size);
    }

    public static void quickSort(File file, int first, int last) throws Exception {
        if (last > first) {
            // Crea nuevo archivo para tener seguimiento
            final String tempFileName = "temp_quick_sort_" + System.currentTimeMillis();
            File tempFile = copyFile(file, tempFileName);

            int pivotIndex = partition(tempFile, file, first, last);
            quickSort(file, first, pivotIndex - 1);
            quickSort(file, pivotIndex + 1, last);
        }
    }

    public static int partition(File readFile, File writeFile, int first, int last) throws Exception {
        // SACAR LA PORCION A ORDENAR
        // Mover Scanner hasta first
        Scanner sc = new Scanner(readFile);
        PrintWriter pw = new PrintWriter(writeFile);
        int pivotIndex = 0;
        int pos = 0;
        while (sc.hasNextInt() && pos < first) {
            // Se pasa datos que no van a ser ordenados directo al archivo final
            pw.println(sc.nextInt());
            pos++;
        }
        pw.flush();
        int pivot = sc.nextInt();

        // pos debe quedar donde está
        // Se crean los archivos temp_low y temp_high
        File temp_low = File.createTempFile("temp_low", ".temp");
        File temp_high = File.createTempFile("temp_high", ".temp");
        temp_low.deleteOnExit();
        temp_high.deleteOnExit();
        PrintWriter pw_low = new PrintWriter(temp_low);
        PrintWriter pw_high = new PrintWriter(temp_high);

        // Se transfieren los elementos al archivo correspondiente comparado con el
        // pivot

        for (int i = pos + 1; i <= last && sc.hasNextInt(); i++) {
            int value = sc.nextInt();

            if (value <= pivot) {
                pw_low.println(value);
            } else {
                pw_high.println(value);
            }
        }
        pw_low.flush();
        pw_high.flush();

        // Se inserta todos los menores
        Scanner sc_low = new Scanner(temp_low);
        while (sc_low.hasNextInt()) {
            pw.println(sc_low.nextInt());
            pos++;
        }

        // Se inserta el pivot
        pw.println(pivot);
        pivotIndex = pos++;
        pw.flush();

        // Se inserta todos los mayores
        Scanner sc_high = new Scanner(temp_high);
        while (sc_high.hasNextInt()) {
            pw.println(sc_high.nextInt());
            pos++;
        }
        pw.flush();

        // Se termina de transferir los demas elementos
        while (sc.hasNextInt()) {
            pw.println(sc.nextInt());
        }

        pw_high.close();
        sc_high.close();
        pw_low.close();
        sc_low.close();
        pw.close();
        sc.close();

        return pivotIndex;
    }

    public static int getValueFromLine(File file, int line) throws Exception {
        int retorno = 0;
        int control = 0;
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine() && control < line) {
            retorno = sc.nextInt();
            control++;
        }
        sc.close();
        return retorno;
    }

    public static File copyFile(File file, String newFileName) throws Exception {
        Path to = Paths.get(newFileName);
        Path from = Paths.get(file.getName());
        Path p = Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
        return p.getFileName().toFile();
        // System.exit(0);
    }
}