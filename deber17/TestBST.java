
public class TestBST {
  public static void main(String[] args) {
    // Create a BST
    // BST<String> tree = new BST<>();
    // tree.insert("George");
    // tree.insert("Michael");
    // tree.insert("Tom");
    // tree.insert("Adam");
    // tree.insert("Jones");
    // tree.insert("Peter");
    // tree.insert("Daniel");
    BST<Carro> tree = new BST<>();
    tree.insert(new Carro("blanco", "pew-2341", 4));
    tree.insert(new Carro("blanco", "paw-2341", 4));
    tree.insert(new Carro("blanco", "pev-2341", 4));
    tree.insert(new Carro("blanco", "pzw-1341", 4));
    tree.insert(new Carro("blanco", "pgw-2651", 4));
    tree.insert(new Carro("blanco", "pep-2365", 4));

    // Traverse tree
    System.out.print("Inorder (sorted): ");
    tree.inorder();
    System.out.print("\nPostorder: ");
    tree.postorder();
    System.out.print("\nPreorder: ");
    tree.preorder();
    System.out.print("\nThe number of nodes is " + tree.getSize());

    // Search for an element
    System.out.print("\nIs (pgw-2651) in the tree? " + tree.search(new Carro("blanco", "pgw-2651", 4)));

    // Get a path from the root to Peter
    System.out.print("\nA path from the root to (pgw-2651) is: ");
    java.util.ArrayList<BST.TreeNode<Carro>> path = tree.path(new Carro("blanco", "pgw-2651", 4));
    for (int i = 0; path != null && i < path.size(); i++)
      System.out.print(path.get(i).element + " ");

    Carro[] carros = { new Carro("blanco", "pew-2341", 4), new Carro("blanco", "paw-2341", 4),
        new Carro("blanco", "pev-2341", 4), new Carro("blanco", "pzw-1341", 4), new Carro("blanco", "pgw-2651", 4),
        new Carro("blanco", "pep-2365", 4) };
    BST<Carro> intTree = new BST<>(carros);
    System.out.print("\nInorder (sorted): ");
    intTree.inorder();
  }
}
