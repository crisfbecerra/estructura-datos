import java.util.*;

public class TestArrayAndLinkedList {
    public static void main(String[] args) {
        List<Carro> arrayList = new ArrayList<>();
        arrayList.add(new Carro("rojo", "automovil", "PCD-564"));
        arrayList.add(new Carro("azul", "automovil", "PPL-453"));
        arrayList.add(new Carro("rojo", "camioneta", "UYT-478"));
        arrayList.add(new Carro("rojo", "automovil", "PCI-106"));
        arrayList.add(new Carro("blanco", "automovil", "CVB-4785"));
        arrayList.add(new Carro("verde", "camioneta", "LOI-4125"));
        arrayList.add(3, new Carro("rojo", "automovil", "PCD-333"));

        System.out.println("A list of integers in the array list:");
        System.out.println(arrayList);

        LinkedList<Object> linkedList = new LinkedList<Object>(arrayList);
        linkedList.add(1, "red");
        linkedList.removeLast();
        linkedList.addFirst("green");

        System.out.println("Display the linked list forward:");
        ListIterator<Object> listIterator = linkedList.listIterator();

        while (listIterator.hasNext()) {
            System.out.print(listIterator.next() + " ");
        }
        System.out.println();

        System.out.println("Display the linked list backward:");
        listIterator = linkedList.listIterator(linkedList.size());

        while (listIterator.hasPrevious()) {
            System.out.print(listIterator.previous() + " ");
        }
    }
}