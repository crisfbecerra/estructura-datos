# Deber 3

Martes 15 de septiembre del 2020

## Uso de la clase ArrayList

Como método para entender el funcionamiento genérico de `ArrayList` se require que se utilice el código encontrado en la página 807, ejercicio 20.4, del libro _"Introduction to Java™ Programming and Data Structures, Comprehensive Version, Eleventh Edition, Global Edition"_. Para esto se utiliza una `clase` propia del usuario, en este caso la clase `Carro` en lugar de la clase `Integer`. EL programa debe correr sin ningún problema, ni requerir ninguna otra modificación.

### Notas

- En efecto el programa funciona sin requerir ninguna modificación adicional
