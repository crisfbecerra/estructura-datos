
import java.util.Collection;

public interface MyList<E extends Comparable<E>> extends Collection<E> {

  /** Add a new element at the specified index in this list */
  public void add(int index, E e);

  /** Return the element from this list at the specified index */
  public E get(int index);

  /**
   * Return the index of the first matching element in this list. Return -1 if no
   * match.
   */
  public int indexOf(Object e);

  /**
   * Return the index of the last matching element in this list Return -1 if no
   * match.
   */
  public int lastIndexOf(E e);

  /**
   * Regresa el index de el primer elemento encontrado que sea igual al objeto
   * pasado, iniciano la busqueda en la posicion de init
   * 
   * @param e
   * @param init
   * @return int
   */
  public int indexOf(E e, int init);

  /**
   * Funcion que ordena los elementos de la lista
   */
  public void sort();

  /**
   * Funcion que devuelve la union de dos MyList
   * 
   * @param MyList<e>
   * @return MyList<e>
   */
  public MyList<E> union(MyList<E> e);

  /**
   * Método que devuelve la interseccion de dos listas
   * 
   * @param MyList<E>
   * @return MyList<E>
   */
  public MyList<E> intersect(MyList<E> e);

  /**
   * Método que devuelve la union de dos MyList ordenada
   * 
   * @param MyList<E>
   * @return MyList<E>
   */
  public MyList<E> merge(MyList<E> e);

  /**
   * Remove the element at the specified position in this list Shift any
   * subsequent elements to the left. Return the element that was removed from the
   * list.
   */
  public E remove(int index);

  /**
   * Replace the element at the specified position in this list with the specified
   * element and returns the new set.
   */
  public E set(int index, E e);

  @Override /** Add a new element at the end of this list */
  public default boolean add(E e) {
    add(size(), e);
    return true;
  }

  @Override /** Return true if this list contains no elements */
  public default boolean isEmpty() {
    return size() == 0;
  }

  @Override /**
             * Remove the first occurrence of the element e from this list. Shift any
             * subsequent elements to the left. Return true if the element is removed.
             */
  public default boolean remove(Object e) {
    if (indexOf(e) >= 0) {
      remove(indexOf(e));
      return true;
    } else
      return false;
  }

  @Override
  public default boolean containsAll(Collection<?> c) {
    // Left as an exercise
    return true;
  }

  @Override
  public default boolean addAll(Collection<? extends E> c) {
    // Left as an exercise
    return true;
  }

  @Override
  public default boolean removeAll(Collection<?> c) {
    // Left as an exercise
    return true;
  }

  @Override
  public default boolean retainAll(Collection<?> c) {
    // Left as an exercise
    return true;
  }

  @Override
  public default Object[] toArray() {
    // Left as an exercise
    return null;
  }

  @Override
  public default <T> T[] toArray(T[] array) {
    // Left as an exercise
    return null;
  }
}