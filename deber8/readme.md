# Deber 8

06 de octubre del 2020

## Contexto

En esta clase se busca estudiar la implementación de un `ArrayList`. Para esto se ha creado una interfaz propia llamada `MyList` que dicta los compotamientos de la clase `MyArrayList`. Este `MyArrayList` utiliza un arreglo de objetos genérico para almacenar los datos.

## Instrucciones

- Agregar a la interfaz `MyList` los siguientes métodos:
  - Método de busqueda de un valor que devuelve el índice del primero objeto que sea igual. Se inicia la busqueda desde la posición indicada. `int indexOf(E e, int start)`.
  - Método que ordena los elementos dentro del arreglo. `void sort()`.
  - Método que une el arreglo actual con el arreglo de otro `MyList` y devuelve un nuevo `MyList`. `Mylist union(MyList e)`.
  - Método que devuelve un `MyList` con la intersección entre dos `MyList`. `Mylist intersect(MyList e)`.
  - Método que devuelve un `MyLst` ordenado, el cual contiene la union de dos `MyList`. `Mylist merge(MyList e)`.

## Notas

- Se requiere sobreescribir el método `equals(Object o)` en la clase `Carro`.
- Para poder realizar el método `sort()` se necesita que el tipo de dato que se va a usar en `MyList` implemente la interfaz `Comparable`.
