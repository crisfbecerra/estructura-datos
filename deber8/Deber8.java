import java.io.*;
import java.util.Scanner;

public class Deber8 {
    public static void main(String[] args) throws Exception {
        final File MOCK_FILE = new File("MOCK_DATA.txt");
        final Scanner scan = new Scanner(MOCK_FILE);
        MyArrayList<Carro> miArreglo = new MyArrayList<>();
        for (int i = 0; i < 10 && scan.hasNextLine(); i++) {
            Carro c = createCarroFromLine(scan.nextLine());
            miArreglo.add(c);
        }
        // Impresion de la lista completa
        System.out.println(miArreglo);

        // Busqueda de indice desde posicion de inicio
        Carro buscar = createCarroFromLine(MOCK_FILE, 3);
        System.out.println("Objeto para busqueda: " + buscar);
        System.out.println("Busqueda inicia en subindice [4]");
        System.out.println("Resultado de busqueda: " + miArreglo.indexOf(buscar, 4));
        System.out.println("Busqueda inicia en subindice [1]");
        System.out.println("Resultado de busqueda: " + miArreglo.indexOf(buscar, 1));

        // Prueba de sort
        System.out.println("\nLista ordenada por placas");
        miArreglo.sort();
        System.out.println(miArreglo);

        // Crendo nueva lista para operaciones
        MyArrayList<Carro> listaParaOperaciones = new MyArrayList<>();
        for (int i = 5; i < 15; i++) {
            listaParaOperaciones.add(createCarroFromLine(MOCK_FILE, i));
        }
        System.out.println("Nueva lista para operaciones");
        System.out.println(listaParaOperaciones);

        // Prueba de union
        System.out.println("\nResultado de union");
        System.out.println(miArreglo.union(listaParaOperaciones));

        // Prueba de interseccion
        System.out.println("\nResultado de interseccion");
        System.out.println(miArreglo.intersect(listaParaOperaciones));

        // Prueba de merge
        System.out.println("\nResultado de merge (Union ordenada)");
        System.out.println(miArreglo.merge(listaParaOperaciones));

        // Cierre del archivo MOC_DATA.txt
        scan.close();

    }

    public static Carro createCarroFromLine(String line) throws Exception {
        Scanner scan = new Scanner(line);
        String placa = scan.next();
        String color = scan.next();
        int n_puertas = scan.nextInt();
        scan.close();
        return new Carro(placa, color, n_puertas);
    }

    public static Carro createCarroFromLine(File file, int line) throws Exception {
        Scanner scan = new Scanner(file);
        Carro c;
        for (int i = 0; i < line && scan.hasNextLine(); i++) {
            scan.nextLine();
        }
        if (scan.hasNextLine()) {
            c = createCarroFromLine(scan.nextLine());
        } else {
            scan.close();
            throw new IndexOutOfBoundsException("No existe la linea " + line);
        }
        scan.close();
        return c;
    }
}
