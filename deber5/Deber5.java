import java.util.*;

/**
 * Deber5
 */
public class Deber5 {

    // Conatantes para estilos de impresion en consola
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    // Lista de colores para generar autos
    private static final List<String> colores = Arrays.asList("blanco", "negro", "rojo", "azul", "verde");

    public static void main(String[] args) {

        Random rand = new Random();
        List<Carro> lista = new ArrayList<>();
        for (int i = 0; i < 10; i++)
            lista.add(generarCarro());

        System.out.println(ANSI_RED + "Lista original" + ANSI_RESET);
        System.out.println(lista);
        System.out.println();

        // Comparable usa el ordenamiento por placa
        Collections.sort(lista);
        System.out.println(ANSI_RED + "Lista ordenada por placa [Usando Comparable]" + ANSI_RESET);
        System.out.println(lista);
        System.out.println();

        // Ordenamieto con el uso de un Comparator
        Collections.sort(lista, new CompararPuertas());
        System.out.println(ANSI_RED + "Lista ordenada por número de puertas [Usando un Comparator]" + ANSI_RESET);
        System.out.println(lista);
        System.out.println();

        /**
         * BinarySearch, busqueda de un objeto dentro de la lista ordenada Se debe
         * reordenar por el método de Comparable, ya que es el mismo método que utiliza
         * binarySearch
         */
        Collections.sort(lista);
        int keyBusqueda = rand.nextInt(lista.size());
        Carro carroParaBuscar = lista.get(keyBusqueda);
        int keyResultado = Collections.binarySearch(lista, carroParaBuscar);
        System.out.println(ANSI_RED + "Busqueda binaria" + ANSI_RESET);
        System.out.println("Elemento buscado: " + carroParaBuscar);
        System.out.println("Index para la busqueda: " + keyBusqueda);
        System.out.println("Resultado de la busqueda: " + keyResultado);
        System.out.println();

        /**
         * BinarySearch con parametro Comparator
         */
        keyBusqueda = rand.nextInt(lista.size());
        carroParaBuscar = lista.get(keyBusqueda);
        keyResultado = Collections.binarySearch(lista, carroParaBuscar, new CompararPlacas());
        System.out.println(ANSI_RED + "Busqueda binaria con Comparator" + ANSI_RESET);
        System.out.println("Elemento buscado: " + carroParaBuscar);
        System.out.println("Index para la busqueda: " + keyBusqueda);
        System.out.println("Resultado de la busqueda: " + keyResultado);
        System.out.println();

        /**
         * Reverse, voltea el orden de una lista.
         */
        Collections.reverse(lista);
        System.out.println(ANSI_RED + "Lista reordenado inverso (Ordenada por placa)" + ANSI_RESET);
        System.out.println(lista);
        System.out.println();

        /**
         * ReverseOrder, devuelve un Comparator con el orden inverso Si se ordena una
         * lista inversa con el Comparator inverso. El comparador inverso devuelve el
         * reverso del comparador natural
         */
        Collections.sort(lista, Collections.reverseOrder(new CompararPuertas()));
        System.out.println(ANSI_RED + "Reordenado con comparador inverso (Por número de puertas)" + ANSI_RESET);
        System.out.println(lista);
        System.out.println();

        /**
         * Shuffle, ordena aleatoreamente la lista de objetos
         */
        Collections.shuffle(lista);
        System.out.println(ANSI_RED + "Lista ordenado aleatoreo" + ANSI_RESET);
        System.out.println(lista);
        System.out.println();

        /**
         * Shuffle con parametro Random, ordena aleatoreamente la lista de objetos, con
         * un objeto random
         */
        Collections.shuffle(lista, rand);
        System.out.println(ANSI_RED + "Lista ordenado aleatoreo usando Random" + ANSI_RESET);
        System.out.println(lista);
        System.out.println();

        /**
         * Copy, hace una copia de la lista SRC en la lista DES. La lista de destino a
         * debe tener la misma cantidad de items que la fuente
         */
        List<Carro> listaDestinoCopia = new ArrayList<>(lista);
        Collections.copy(listaDestinoCopia, lista);
        System.out.println(ANSI_RED + "Copia de una lista en otra" + ANSI_RESET);
        System.out.println(ANSI_GREEN + "Lista original");
        System.out.println(lista);
        System.out.println(ANSI_RESET);
        System.out.println(ANSI_BLUE + "Lista copiada");
        System.out.println(listaDestinoCopia);
        System.out.println(ANSI_RESET);
        System.out.println();

        /**
         * nCopies devuelve una lista llena de n veces el Objeto
         */
        List<Carro> listaDeCopias = Collections.nCopies(4, generarCarro());
        System.out.println(ANSI_RED + "Lista de copias" + ANSI_RESET);
        System.out.println(listaDeCopias);
        System.out.println();

        /**
         * fill llena una lista dada con el objeto dado. fill utiliza el metodo set()
         * que esta implementado en AbstractList
         */
        Collections.fill(listaDestinoCopia, generarCarro());
        System.out.println(ANSI_RED + "Lista llena con un solo objeto" + ANSI_RESET);
        System.out.println(listaDestinoCopia);
        System.out.println();

        /**
         * Variable para guardar datos devuelto por lo siguientes métodos
         */
        Carro objetoDevuelto;

        /**
         * max, devuelve el mayor objeto de la coleccion basado en la interfaz
         * Comparable
         */
        System.out.println(ANSI_RED + "Busqueda de máximos y mínimos" + ANSI_RESET);
        Collections.shuffle(lista);
        objetoDevuelto = Collections.max(lista);
        Collections.sort(lista);
        System.out.println(ANSI_RED + "Lista:" + ANSI_RESET);
        System.out.println(lista);
        System.out.println(ANSI_BLUE + "Max (placa): " + objetoDevuelto + ANSI_RESET);
        /**
         * Max con Comparator, devuelve el mayo basado en la interfaz Comparador
         */
        objetoDevuelto = Collections.max(lista, new CompararPuertas());
        System.out.println(ANSI_BLUE + "Max (Comparator - puertas): " + objetoDevuelto + ANSI_RESET);
        /**
         * min, devuelve el manor objeto de la coleccion basado en la interfaz
         * Comparable
         */
        objetoDevuelto = Collections.min(lista);
        System.out.println(ANSI_GREEN + "Min (placa): " + objetoDevuelto + ANSI_RESET);
        /**
         * Min con Comparator, devuelve el menor basado en la interfaz Comparador
         */
        objetoDevuelto = Collections.min(lista, new CompararPuertas());
        System.out.println(ANSI_GREEN + "Min (Comparator - puertas): " + objetoDevuelto + ANSI_RESET);
        System.out.println();

        /**
         * DistJoin, verifica si dos listas comparten items. Devuelve un boolean si
         * comparten items
         */
        System.out.println(ANSI_RED + "Verificar si dos listas comparten elementos" + ANSI_RESET);
        if (Collections.disjoint(lista, listaDestinoCopia)) {
            System.out.println("Las listas NO comparten items");
        } else {
            System.out.println("Las listas SI comparten items");
        }
        System.out.println("Se agrega un item de una lista en otra");
        // Se agrega un item a otra para ver diferencia
        listaDestinoCopia.add(lista.get(rand.nextInt(lista.size())));
        if (Collections.disjoint(lista, listaDestinoCopia)) {
            System.out.println("Las listas NO comparten items");
        } else {
            System.out.println("Las listas SI comparten items");
        }
        System.out.println();

        /**
         * Frequency, devuelve el número de veces que un objeto es encontrado en una
         * lista
         */
        System.out.println(ANSI_RED + "Busqueda de frecuecia de un elemento en la lista" + ANSI_RESET);
        objetoDevuelto = lista.get(rand.nextInt(lista.size()));
        System.out.println(ANSI_GREEN + "Objeto buscado -> " + objetoDevuelto + ANSI_RESET);
        System.out.println(
                ANSI_BLUE + "Veces encontrado en lista: " + Collections.frequency(lista, objetoDevuelto) + ANSI_RESET);
        int n_veces = rand.nextInt(5) + 1;
        for (int i = 0; i < n_veces; i++)
            lista.add(objetoDevuelto);
        System.out.println("Objeto agregado " + n_veces + " veces");
        System.out.println(
                ANSI_BLUE + "Veces encontrado en lista: " + Collections.frequency(lista, objetoDevuelto) + ANSI_RESET);
        System.out.println("Lista actual:");
        System.out.println(lista);
        System.out.println();
    }

    /**
     * Funcion genera un string con el formato AAA-#### para simular una placa de
     * auto
     * 
     * @return String
     */
    private static String generarPlacar() {
        StringBuilder placa = new StringBuilder(8);
        Random r = new Random();
        for (int i = 0; i < 3; i++) {
            placa.append((char) (r.nextInt(25) + 65));
        }
        placa.append("-");
        for (int i = 0; i < 4; i++) {
            placa.append(r.nextInt(10));
        }

        return placa.toString();
    }

    /**
     * Funcion genera un nuevo objeto Carro con parametros aleatoreos
     * 
     * @return Carro
     */
    private static Carro generarCarro() {
        Random rand = new Random();
        return new Carro(colores.get(rand.nextInt(colores.size())), generarPlacar(), rand.nextInt(6) + 1);
    }
}