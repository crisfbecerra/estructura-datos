# Deber 5

22 de Septiembre del 2020

## Uso de métodos estáticos (`Static`) de la clase Collection.

La clase `Collection`, de la cual hereda todas las estructuras de datos de Java, implementa una serie de métodos estáticos los cuales pueden ser usados para distintas operaciones.Estas operaciones se las ejecuta sobre un `list` pasado como parametro a la función.

Algunos de estos métodos son:

- sort: Para el ordenamiento de la `list`.
- searchBinary: Para la busqueda binaria de un elemento.
- shuffle: Para el ordenamiento aleatereo de la `list`.

### Instrucciones

- Crear una `List` de objetos de la clase usuario.
- Probar todos los métodos estáticos `static` de `Collection` en la lista de objetos personalizados.

### Notas

- Los métodos que no requieren un `Comparator` utilizan el método `compareTo` de la interfaz `Comparable`. Por lo tanto esta interfaz debe estar implementada en la clase que se usa.
- `Collections.copy()`: La lista de destino a debe tener la misma cantidad de items que la fuente.
