
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;

public interface MyList<E> extends Collection<E> {
  /** Add a new element at the specified index in this list */
  public void add(int index, E e);

  /** Return the element from this list at the specified index */
  public E get(int index);

  /**
   * Return the index of the first matching element in this list. Return -1 if no
   * match.
   */
  public int indexOf(Object e);

  /**
   * Return the index of the last matching element in this list Return -1 if no
   * match.
   */
  public int lastIndexOf(E e);

  /**
   * Remove the element at the specified position in this list Shift any
   * subsequent elements to the left. Return the element that was removed from the
   * list.
   */
  public E remove(int index);

  /**
   * Replace the element at the specified position in this list with the specified
   * element and returns the new set.
   */
  public E set(int index, E e);

  @Override /** Add a new element at the end of this list */
  public default boolean add(E e) {
    add(size(), e);
    return true;
  }

  @Override /** Return true if this list contains no elements */
  public default boolean isEmpty() {
    return size() == 0;
  }

  @Override /**
             * Remove the first occurrence of the element e from this list. Shift any
             * subsequent elements to the left. Return true if the element is removed.
             */
  public default boolean remove(Object e) {
    if (indexOf(e) >= 0) {
      remove(indexOf(e));
      return true;
    } else
      return false;
  }

  @Override
  public default boolean containsAll(Collection<?> c) {
    // Left as an exercise
    if (size() <= 0 || c.size() <= 0) {
      return false;
    }
    Class itemForCompare = get(0).getClass();
    for (Object item : c) {
      if (!itemForCompare.isInstance(item)) {
        return false;
      } else if (indexOf(item) == -1) {
        return false;
      }
    }
    return true;
  }

  @Override
  public default boolean addAll(Collection<? extends E> c) {
    // Left as an exercise
    for (E item : c) {
      add(item);
    }
    return true;
  }

  @Override
  public default boolean removeAll(Collection<?> c) {
    // Left as an exercise
    for (Object item : c) {
      while (remove(item)) {

      }
    }
    return true;
  }

  @Override
  public default boolean retainAll(Collection<?> c) {
    // Left as an exercise

    boolean retorno = false;
    Iterator<E> iterator = iterator();
    while (iterator.hasNext()) {
      if (!c.contains(iterator.next())) {
        iterator.remove();
        retorno = true;
      }
    }
    return retorno;
  }

  @Override
  public default Object[] toArray() {
    // Left as an exercise
    Object[] retorno = new Object[size()];
    int index = 0;
    Iterator iterator = iterator();
    while (iterator.hasNext()) {
      retorno[index++] = iterator.next();
    }
    return retorno;
  }

  @Override
  public default <T> T[] toArray(T[] array) {
    // Left as an exercise
    T[] retorno = array;
    if (retorno.length < size()) {
      retorno = (T[]) Array.newInstance(array.getClass().getComponentType(), size());
    }
    Iterator iterator = iterator();
    for (int i = 0; i < retorno.length; i++) {
      if (iterator.hasNext())
        retorno[i] = (T) iterator.next();
      else
        retorno[i] = null;
    }
    return retorno;
  }
}