import java.util.Iterator;

public class TestMyLinkedList {
  /** Main method */
  public static void main(String[] args) {
    // Create a list for strings
    MyLinkedList<String> list = new MyLinkedList<String>();

    // Add elements to the list
    list.add("America"); // Add it to the list
    System.out.println("(1) " + list);

    list.add(0, "Canada"); // Add it to the beginning of the list
    System.out.println("(2) " + list);

    list.add("Russia"); // Add it to the end of the list
    System.out.println("(3) " + list);

    list.addLast("France"); // Add it to the end of the list
    System.out.println("(4) " + list);

    list.add(2, "Germany"); // Add it to the list at index 2
    System.out.println("(5) " + list);

    list.add(5, "Norway"); // Add it to the list at index 5
    System.out.println("(6) " + list);

    list.add(0, "Poland"); // Same as list.addFirst("Poland")
    System.out.println("(7) " + list);

    // Remove elements from the list
    list.remove(0); // Same as list.remove("Australia") in this case
    System.out.println("(8) " + list);

    list.remove(2); // Remove the element at index 2
    System.out.println("(9) " + list);

    list.remove(list.size() - 1); // Remove the last element
    System.out.print("(10) " + list + "\n(11) ");

    for (String s : list)
      System.out.print(s.toUpperCase() + " ");

    // Prueba de Contains
    System.out.println();
    System.out.println("Contains 'Russia'? " + list.contains("Russia"));
    System.out.println("Contains 'France'? " + list.contains("France"));
    System.out.println();
    System.out.println("Get index 2: " + list.get(2));
    System.out.println("Get index 4: " + list.get(4));

    // Prueba de index of
    System.out.println();
    System.out.println("Buscar index de 'America': " + list.indexOf("America"));
    System.out.println("Buscar index de 'Ecuador': " + list.indexOf("Ecuador"));

    // Prueba de last index of
    System.out.println();
    System.out.println("Last index of 'Russia': " + list.lastIndexOf("Russia"));
    System.out.println("Agregado 'Russia' al final");
    list.add("Russia");
    System.out.println("Last index of 'Russia': " + list.lastIndexOf("Russia"));

    // Peruaba de set
    System.out.println();
    System.out.println("Set 'Ecuador' in position 1");
    System.out.println("Value returned: " + list.set(1, "Ecuador"));
    System.out.println(list);

    // Prueba de Iterator.remove()
    System.out.println();
    System.out.println("Eliminar 'Russia' utilizando iterator");
    Iterator<String> iterator = list.iterator();
    while (iterator.hasNext()) {
      String item = iterator.next();
      if (item.equals("Russia")) {
        iterator.remove();
      }
    }
    System.out.println("Resultado: " + list);

    // Prueba de containsAll
    MyList<String> lista2 = new MyLinkedList<>();
    lista2.add("Canada");
    lista2.add("Ecuador");
    lista2.add("France");
    lista2.add("Quito");
    System.out.println();
    System.out.println("Lista para busqueda " + lista2);
    System.out.println("List containsAll: " + list.containsAll(lista2));

    // Prueba de add all
    System.out.println();
    System.out.println("Agregar lista2 a list");
    list.addAll(lista2);
    System.out.println(list);

    // Prueba de removeAll
    System.out.println();
    System.out.println("Remover los items de lista2 de list");
    MyList<String> l3 = new MyLinkedList<String>();
    l3.add("Quito");
    l3.add("Canada");
    System.out.println("Items para remover: " + l3);
    list.removeAll(l3);
    System.out.println(list);

    // Prueba de retainAll
    list.addAll(lista2);
    System.out.println();
    System.out.println("Prueba retainAll");
    System.out.println("Se agrega toda la lista2");
    System.out.println(list);
    System.out.println("Retain con " + l3);
    list.retainAll(l3);
    System.out.println("Resultado " + list);

    // Prueba de toArray()
    Object[] arreglo = list.toArray();
    System.out.println();
    System.out.println("Prueba toArray()");
    System.out.println("Array devuelto: ");
    for (int i = 0; i < arreglo.length; i++) {
      System.out.print(arreglo[i] + ", ");
    }

    // Prueba de toArrat(T)
    String[] arreglo2 = new String[1];
    arreglo2 = list.toArray(arreglo2);
    System.out.println();
    System.out.println("Prueba toArray(T)");
    System.out.println("Array devuelto:");
    for (String i : arreglo2) {
      System.out.print(i + ", ");
    }

    // Prueba de limpieza
    System.out.println();
    list.clear();
    System.out.println("\nAfter clearing the list, the list size is " + list.size());
  }
}