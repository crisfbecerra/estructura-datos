# Deber 11

22 de octubre del 2020

## Instrucciones

Crear un programa que obtiene el tiempo de ejecución para encontrar todos los números primos menores que 9,000,000, 11,000,000, 13,000,000, 15,000,000, 17,000,000, y 19,000,000. Se debe usar los algoritmos de los ejercicios 22.5 - 22.7. Imprimir la siguiente tabla:
||9000000|11000000|13000000|15000000|17000000|19000000|
|---|---|----|-----|-----|-----|-----|
|22.5|||||||
|22.6|||||||
|22.7|||||||
