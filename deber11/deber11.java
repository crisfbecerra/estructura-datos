class deber11 {
    public static void main(String[] args) {
        int[] n = { 9000000, 11000000, 13000000, 15000000, 17000000, 19000000 };
        long[][] resultados = new long[n.length][3];
        // PrimeNumbers.primeNumbers(1000);
        for (int i = 0; i < n.length; i++) {
            // Medición de algoritmo de PrimeNumbers
            long startTime = java.lang.System.currentTimeMillis();
            PrimeNumbers.primeNumbers(n[i]);
            resultados[i][0] = java.lang.System.currentTimeMillis() - startTime;
            System.out.println("Tiempo necesario: " + resultados[i][0] + " ms");

            // Medición de algoritmo EfficientPrimeNumbers
            startTime = java.lang.System.currentTimeMillis();
            EfficientPrimeNumbers.efficientPrimeNumbers(n[i]);
            resultados[i][1] = java.lang.System.currentTimeMillis() - startTime;
            System.out.println("Tiempo necesario: " + resultados[i][1] + " ms");

            // Medición de algoritmo SieveOfEratosthenes
            startTime = java.lang.System.currentTimeMillis();
            SieveOfEratosthenes.sieveOfEratosthenes(n[i]);
            resultados[i][2] = java.lang.System.currentTimeMillis() - startTime;
            System.out.println("Tiempo necesario: " + resultados[i][2] + " ms");
        }

        // Impresion de la tabla
        String divisor = "------------------------------------------------------------";
        String format = "% 8d ";
        System.out.println(divisor);
        System.out.printf("%5s ", " ");
        for (int i : n) {
            System.out.format("% 8d", i);
        }
        System.out.println();
        System.out.println(divisor);
        System.out.printf("%5s ", "22.5");
        for (int i = 0; i < n.length; i++) {
            System.out.format(format, resultados[i][0]);
        }
        System.out.println();
        System.out.printf("%5s ", "22.6");
        for (int i = 0; i < n.length; i++) {
            System.out.format(format, resultados[i][1]);
        }
        System.out.println();
        System.out.printf("%5s ", "22.7");
        for (int i = 0; i < n.length; i++) {
            System.out.format(format, resultados[i][2]);
        }

    }

}