import java.util.*;
import java.io.*;

class deber7 {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.print("Nombre de archivo java: ");
        String fileName = input.nextLine();
        input.close();

        File file = new File(fileName);
        if (file.exists()) {
            System.out.println("Archivo abierto");
            countKeywords(file);
        } else {
            System.err.println("Archivo " + fileName + " no encontrado");
        }
    }

    public static void countKeywords(File file) throws Exception {
        String[] keywordString = { "abstract", "assert", "boolean", "break", "byte", "case", "catch", "char", "class",
                "const", "continue", "default", "do", "double", "else", "enum", "extends", "for", "final", "finally",
                "float", "goto", "if", "implements", "import", "instanceof", "int", "interface", "long", "native",
                "new", "package", "private", "protected", "public", "return", "short", "static", "strictfp", "super",
                "switch", "synchronized", "this", "throw", "throws", "transient", "try", "void", "volatile", "while",
                "true", "false", "null" };
        Map<String, Integer> kywordsMap = new TreeMap<>();
        for (String key : keywordString) {
            kywordsMap.put(key, 0);
        }
        Scanner input = new Scanner(file);
        while (input.hasNextLine()) {
            String line = input.nextLine().toLowerCase();
            String[] words = line.split("[\\s+\\p{P}]");
            for (String word : words) {
                if (kywordsMap.containsKey(word)) {
                    int t = kywordsMap.get(word);
                    kywordsMap.put(word, ++t);
                }
            }
        }
        input.close();
        kywordsMap.forEach((key, value) -> System.out.println(key + "\t\t" + value));
    }
}