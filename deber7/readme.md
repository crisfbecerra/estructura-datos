# Deber 7

01 de octubre del 2020

## Maps

La estructura de `Maps` se basa en el sistema clave - valor, se inserta la clave y el valor correspondiente. La `clave` puede ser cualquier cosa, sin embargo la clave siempre debe ser única en comparaciones. Los `valores` pueden ser cualquier cosa y no necesita ser única.

## Instrucciones

- Crear un programa que cuente la cantidad de palabras reservadas del lenguaje `Java`.
- Se debe proveer el nombre del archivo .java que se va a analizar.
- Se debe utilizar una combinación del ejercicio 21.9

## Notas
