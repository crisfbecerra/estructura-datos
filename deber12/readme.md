# Deber 12

27 de octubre del 2020

## Instrucciones

Crear un programa para instrumentar los algoritmos de ordenamiento vistos en clase. Los algoritmo de ordenamiento están en los archivos adjuntos en este repositorio.

- Leer la cantidad de datos que iran en el arreglo a ordenar desde consola.
- Generar un arreglo con n elementos, los valores son generados de manera aleatorea.
- Leer la cantidad de veces que se debe correr la prueba desde consola.
- En cada algoritmo:
  - Contar el número de veces que se realiza la comparación.
  - Contar el número de veces que se realiza el intercambio de ordenamiento.
- Repetir los test de las veces que se paso en consola.
- Imprimir datos relevantes:
  - Promedio de veces que se realizó la comparación en cada algoritmo.
  - Promedio de veces que se realizó el intercambio en cada algoritmo.

## Notas

Notas de desarrollo: Ideas encontradas durante el desarrollo de la tarea.

- El arreglo debe clonarse para ser arregldo por cada algoritmo ya que la posición de los elemetos debe ser igual para todos los algoritmos.
- Se debe modificar los agoritmos originales obtenidos del libro para instrumentarlos.
- Si se require mas de una prueba entonces se debe recalcular los arreglos.
