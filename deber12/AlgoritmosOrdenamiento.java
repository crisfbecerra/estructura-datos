public class AlgoritmosOrdenamiento {
    public static int[] bubbleSort(int[] list) {
        boolean needNextPass = true;
        int comparaciones = 0;
        int intercambios = 0;

        for (int k = 1; k < list.length && needNextPass; k++) {
            // Array may be sorted and next pass not needed
            needNextPass = false;
            for (int i = 0; i < list.length - k; i++) {
                comparaciones++;
                if (list[i] > list[i + 1]) {
                    // Swap list[i] with list[i + 1]
                    int temp = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = temp;
                    intercambios++;

                    needNextPass = true; // Next pass still needed
                }
            }
        }

        return new int[] { comparaciones, intercambios };
    }

    public static int[] insertionSort(int[] list) {
        int comparaciones = 0;
        int intercambios = 0;
        for (int i = 1; i < list.length; i++) {
            /**
             * insert list[i] into a sorted sublist list[0..i-1] so that list[0..i] is
             * sorted.
             */
            int currentElement = list[i];
            int k;
            for (k = i - 1; k >= 0 && list[k] > currentElement; k--) {
                list[k + 1] = list[k];
                comparaciones++;
            }

            // Insert the current element into list[k+1]
            list[k + 1] = currentElement;
            if (k + 1 != i)
                intercambios++;
        }
        return new int[] { comparaciones, intercambios };
    }

    // public static void test() {
    // System.out.println("Metodos de prueba");
    // }
}
