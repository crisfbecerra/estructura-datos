# Deber 1

Jueves, 03 de Septiembre del 2020

## Tipos genéricos

Este deber consiste en crear una clase propia para ser usada dentro de la clase genérica `GenericStack`.
En TestStack se debe crear una instancia de GenericStack que contenga elementos de la clase propia `Carro` y debe se usarse todos los metodos de la clase `GenericStack`.

La clase propia que se usa en este deber es la clase `Carro` que tiene las propidades `modelo`, `color` y `placa` con sus respectivos `set` y `get`. Ademas se sobreescribe el metodo `toString` heredado de la clase `Object`.
