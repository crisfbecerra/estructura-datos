/**
 * Carro
 */
public class Carro {
    private String color;
    private String modelo;
    private String placa;

    public Carro setColor(final String color) {
        this.color = color;
        return this;
    }

    public Carro setModelo(final String modelo) {
        this.modelo = modelo;
        return this;
    }

    public Carro setPlaca(final String placa) {
        this.placa = placa;
        return this;
    }

    public String getColor() {
        return this.color;
    }

    public String getModelo() {
        return this.modelo;
    }

    public String getPlaca() {
        return this.placa;
    }

    @Override
    public String toString() {
        return "Carro: => Color: " + this.getColor() + ", Modelo: " + this.getModelo() + ", Placa: " + this.getPlaca();
    }
}