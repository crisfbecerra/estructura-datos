
public class TestStack {

  public static void main(String[] args) {

    GenericStack<Carro> carros = new GenericStack<>();

    carros.push(new Carro().setColor("Rojo").setPlaca("PBY-1254").setModelo("Automovil"));
    carros.push(new Carro().setColor("Verde").setPlaca("PBZ-0274").setModelo("Camioneta"));
    carros.push(new Carro().setColor("Blanco").setPlaca("GHP-365").setModelo("Automovil"));
    System.out.println(carros);

    if (!carros.isEmpty()) {
      System.out.println("El stack carros no esta vacio");
      System.out.println("El stack carros contiene " + carros.getSize() + " elementos");
      System.out.println("Ultimo elemento agregado: " + carros.peek());
    }

    while (!carros.isEmpty()) {
      System.out.println("Ultimo elemento extraido: " + carros.pop());
    }
    System.out.println("Stack carros esta vacio");
  }
}