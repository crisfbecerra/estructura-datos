import java.util.*;
import java.io.File;

/**
 * Deber 6.
 */

public class SetListPerformanceTest {
  static final int N = 50000;
  static final String DATAFILENAME = "MOCK_DATA.txt";
  static List<Carro> list = new ArrayList<>();

  public static void main(String[] args) {
    // Se verifica la existencia el archivo
    File dataFile = new File(DATAFILENAME);
    if (!dataFile.exists()) {
      System.out.println("Archivo de datos (MOCK_DATA.txt) no encontrado");
      System.exit(0);
    }

    // List<Carro> list = new ArrayList<>();

    // Lectura de archivo
    /**
     * El archivo de datos sigue la estructura |color| |Placa| |Número de puertas|
     */
    try (Scanner fileInput = new Scanner(dataFile);) {
      while (fileInput.hasNext()) {
        list.add(new Carro(fileInput.next(), fileInput.next(), fileInput.nextInt()));
      }
      fileInput.close();
    } catch (NoSuchElementException e) {
      System.out.println("Error en la estructura del archivo de datos");
      System.exit(0);
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
      System.exit(0);
    } catch (Exception e) {
      System.out.println("Error al leer archivo");
      System.exit(0);
    }
    Collections.shuffle(list); // Shuffle the array list

    // Create a hash set, and test its performance
    Collection<Carro> set1 = new HashSet<>(list);
    System.out.println("Member test time for hash set is " + getTestTime(set1) + " milliseconds");
    System.out.println("Remove element time for hash set is " + getRemoveTime(set1) + " milliseconds");

    // Create a linked hash set, and test its performance
    Collection<Carro> set2 = new LinkedHashSet<>(list);
    System.out.println("Member test time for linked hash set is " + getTestTime(set2) + " milliseconds");
    System.out.println("Remove element time for linked hash set is " + getRemoveTime(set2) + " milliseconds");

    // Create a tree set, and test its performance
    Collection<Carro> set3 = new TreeSet<>(list);
    System.out.println("Member test time for tree set is " + getTestTime(set3) + " milliseconds");
    System.out.println("Remove element time for tree set is " + getRemoveTime(set3) + " milliseconds");

    // Create an array list, and test its performance
    Collection<Carro> list1 = new ArrayList<>(list);
    System.out.println("Member test time for array list is " + getTestTime(list1) + " milliseconds");
    System.out.println("Remove element time for array list is " + getRemoveTime(list1) + " milliseconds");

    // Create a linked list, and test its performance
    Collection<Carro> list2 = new LinkedList<>(list);
    System.out.println("Member test time for linked list is " + getTestTime(list2) + " milliseconds");
    System.out.println("Remove element time for linked list is " + getRemoveTime(list2) + " milliseconds");
  }

  public static long getTestTime(Collection<Carro> c) {
    long startTime = System.currentTimeMillis();

    // Test if a car is in the collection
    for (int i = 0; i < list.size(); i++) {
      c.contains(list.get((int) (Math.random() * list.size())));
    }

    return System.currentTimeMillis() - startTime;
  }

  public static long getRemoveTime(Collection<Carro> c) {
    List<Carro> temp = new ArrayList<>(c);
    Collections.shuffle(temp);

    long startTime = System.currentTimeMillis();

    for (int i = 0; i < temp.size(); i++)
      c.remove(temp.get(i));

    return System.currentTimeMillis() - startTime;
  }
}