# Deber 6

30 de septiembre del 2020

## Sets

Las estructuras de datos de tipo `Set` son estructuras que no aceptan duplicados. En java hay varias implementaciones de `Set` como `HashSet`, `LinkedSet` y `TreeSet`. Cada una de estas implementaciones tiene sus propias lógicas y diferencias.

Los `HashSet` son contenedores sin un orden, son los más eficientes, Los `LinkedSet` son contenedores donde el orden de entrada se mantiene, cada elemento sabe que elemento es el siguiente. `TreeSet` son contenedores que mantienen el orden de acuerdo a la interfaz `Comparable` del objeto suministrado o a través de un `Comparator` suministrado en el `Constructor`.

**Factor de carga**: Es la capacidad de un `Set` para almacenar los objetos. Es el número de objetos que se puede almacenar en el `Set`.

## Instrucciones

- Utilizar el programa cliente _SetListPerformanceTest_ con la clase de usuario (`Carro`).
- Se debe crear al menos 20 instancias del la clase de usuario.
- Los datos para crear las instancias deben ser leidas desde un archivo de texto adjunto al programa.

## Notas de desarrollo

- Se utilizo los servicios de la página web [Mockaroo.com](https://www.mockaroo.com/schemas/download) para obtener un MOCK de datos para el programa.
- Se necesito 1000 registros para que el tiempo de test sea mayor a 0 milisegundos.
