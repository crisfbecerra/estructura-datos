import java.util.Comparator;

public class CompararPuertas implements Comparator<Carro> {

    @Override
    /**
     * Esta ccompara el numero de puertas de un carro
     * 
     * @param Carro o1
     * @param Carro o2
     * @return int
     */
    public int compare(Carro o1, Carro o2) {
        if (o1.getPuertas() > o2.getPuertas()) {
            return 1;
        } else if (o1.getPuertas() < o2.getPuertas()) {
            return -1;
        }
        return 0;
    }

}
