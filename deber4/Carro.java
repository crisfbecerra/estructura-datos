
/**
 * Carro
 */
public class Carro {
    private String color;
    private String placa;
    private int n_puertas;

    public Carro(final String color, final String placa, final int n_puertas) {
        setColor(color);

        setPlaca(placa);
        setPuertas(n_puertas);
    }

    public Carro setColor(final String color) {
        this.color = color;
        return this;
    }

    // public Carro setModelo(final String modelo) {
    // this.modelo = modelo;
    // return this;
    // }

    public Carro setPlaca(final String placa) {
        this.placa = placa;
        return this;
    }

    public Carro setPuertas(int puertas) {
        if (puertas <= 0) {
            throw new java.lang.IllegalArgumentException();
        }
        this.n_puertas = puertas;
        return this;
    }

    public String getColor() {
        return this.color;
    }

    // public String getModelo() {
    // return this.modelo;
    // }

    public String getPlaca() {
        return this.placa;
    }

    public int getPuertas() {
        return this.n_puertas;
    }

    @Override
    public String toString() {
        // return "Carro: => Color: " + this.getColor() + ", Modelo: " +
        // this.getModelo() + ", Placa: " + this.getPlaca();
        return "Carro: => Color: " + this.getColor() + ", Puertas: " + this.getPuertas() + ", Placa: "
                + this.getPlaca();
    }
}