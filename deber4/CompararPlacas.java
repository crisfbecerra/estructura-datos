import java.util.Comparator;

public class CompararPlacas implements Comparator<Carro> {

    @Override
    public int compare(Carro o1, Carro o2) {
        return o1.getPlaca().compareTo(o2.getPlaca());
    }

}
