# Deber 4

18 de Septiembre del 2020

## Uso de interfaz Comparator

Para este deber se debe utilizar la interfaz funcional `Comparator` para comparar los objetos de la clase `Carro`.

### Instrucciones

- Crear dos `Comparator` para comparar distintas propiedades de los objetos pertenecientes a la clase `Carro`.
- Para probar los `Comparator` se debe usar el código del ejemplo 20.8 del libro _"Introduction to Java™ Programming and Data Structures, Comprehensive Version, Eleventh Edition, Global Edition"_

### Notas

- Al usar el `Comparator` se elimina el uso de la función `Lambda` como `callback` en la función `sort` del objeto `List`
