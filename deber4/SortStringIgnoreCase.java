public class SortStringIgnoreCase {
    public static void main(String[] args) {
        java.util.List<Carro> cities = java.util.Arrays.asList(new Carro("rojo", "PBV-1523", 4),
                new Carro("blanco", "PPV-1743", 5), new Carro("azul", "PUY-1143", 3));

        // Comparado por numero de puertas. Se ordena por número de puertas de menor a
        // mayor
        cities.sort(new CompararPuertas());
        System.out.println("Ordenado por número de puertas");
        for (Carro s : cities) {
            System.out.println(s + " ");
        }

        // Comparado por placas. Se ordena alfabeticamente
        cities.sort(new CompararPlacas());
        System.out.println("Ordenado alfabético de placas");
        for (Carro s : cities) {
            System.out.println(s);
        }
    }
}