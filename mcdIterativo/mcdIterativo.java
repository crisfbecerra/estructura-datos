class mcdIterativo {
    public static void main(String[] args) {
        System.out.println("MCD: " + gcd(75, 570));
    }

    public static int gcd(int m, int n) {
        int r = m % n;
        while (r != 0) {
            m = n;
            n = r;
            r = m % n;
        }
        return n;

    }
}