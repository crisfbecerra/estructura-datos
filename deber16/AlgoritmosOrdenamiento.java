/**
 * @author Cristopher Becerra
 */
public class AlgoritmosOrdenamiento {
    /**
     * Algortimo de ordenamiento Burbuja
     * 
     * @param list
     * @return
     */
    public static int[] bubbleSort(int[] list) {
        boolean needNextPass = true;
        int comparaciones = 0;
        int intercambios = 0;

        for (int k = 1; k < list.length && needNextPass; k++) {
            // Array may be sorted and next pass not needed
            needNextPass = false;
            for (int i = 0; i < list.length - k; i++) {
                comparaciones++;
                if (list[i] > list[i + 1]) {
                    // Swap list[i] with list[i + 1]
                    int temp = list[i];
                    list[i] = list[i + 1];
                    list[i + 1] = temp;
                    intercambios++;

                    needNextPass = true; // Next pass still needed
                }
            }
        }

        return new int[] { comparaciones, intercambios };
    }

    /**
     * Algoritmo de ordenamiento insercion
     * 
     * @param list
     * @return
     */
    public static int[] insertionSort(int[] list) {
        int comparaciones = 0;
        int intercambios = 0;
        for (int i = 1; i < list.length; i++) {
            /**
             * insert list[i] into a sorted sublist list[0..i-1] so that list[0..i] is
             * sorted.
             */
            int currentElement = list[i];
            int k;
            for (k = i - 1; k >= 0 && list[k] > currentElement; k--) {
                list[k + 1] = list[k];
                comparaciones++;
            }

            // Insert the current element into list[k+1]
            list[k + 1] = currentElement;
            if (k + 1 != i)
                intercambios++;
        }
        return new int[] { comparaciones, intercambios };
    }

    /**
     * Algoritmo de ordenamiento merge
     * 
     * @param int[] list
     */
    public static int[] mergeSort(int[] list) {
        int comparaciones = 0, intercambios = 0;
        int[] mitad1 = { 0, 0 }, mitad2 = { 0, 0 }, desdeMerge = { 0, 0 };
        if (list.length > 1) {
            // Merge sort the first half
            int[] firstHalf = new int[list.length / 2];
            System.arraycopy(list, 0, firstHalf, 0, list.length / 2);
            mitad1 = mergeSort(firstHalf);

            // Merge sort the second half
            int secondHalfLength = list.length - list.length / 2;
            int[] secondHalf = new int[secondHalfLength];
            System.arraycopy(list, list.length / 2, secondHalf, 0, secondHalfLength);
            mitad2 = mergeSort(secondHalf);

            // Merge firstHalf with secondHalf into list
            desdeMerge = merge(firstHalf, secondHalf, list);
        }
        return new int[] { comparaciones + mitad1[0] + mitad2[0] + desdeMerge[0],
                intercambios + mitad1[1] + mitad2[1] + desdeMerge[1] };
    }

    /**
     * Union de las listas donde se ordenan
     * 
     * @param list1
     * @param list2
     * @param temp
     */
    private static int[] merge(int[] list1, int[] list2, int[] temp) {
        int current1 = 0; // Current index in list1
        int current2 = 0; // Current index in list2
        int current3 = 0; // Current index in temp
        int comparaciones = 0;
        int intercambios = 0;

        while (current1 < list1.length && current2 < list2.length) {
            comparaciones++;
            intercambios++;
            if (list1[current1] < list2[current2])
                temp[current3++] = list1[current1++];
            else
                temp[current3++] = list2[current2++];
        }

        while (current1 < list1.length)
            temp[current3++] = list1[current1++];

        while (current2 < list2.length)
            temp[current3++] = list2[current2++];

        return new int[] { comparaciones, intercambios };
    }

    /**
     * Algoritmo de ordenamiento quick
     * 
     * @param list
     */
    public static int[] quickSort(int[] list) {
        return quickSort(list, 0, list.length - 1);
    }

    private static int[] quickSort(int[] list, int first, int last) {
        if (last > first) {
            int[] pivotIndex = partition(list, first, last);
            int[] mitad1 = quickSort(list, first, pivotIndex[2] - 1);
            int[] mitad2 = quickSort(list, pivotIndex[2] + 1, last);
            return new int[] { pivotIndex[0] + mitad1[0] + mitad2[0] + 1, pivotIndex[1] + mitad1[1] + mitad2[1] };
        }
        return new int[] { 1, 0 };
    }

    /** Partition the array list[first..last] */
    private static int[] partition(int[] list, int first, int last) {
        int pivot = list[first]; // Choose the first element as the pivot
        int low = first + 1; // Index for forward search
        int high = last; // Index for backward search
        int comparaciones = 1;
        int intercambios = 0;

        while (high > low) {
            comparaciones++;// Comparacion del while externo
            // Search forward from left
            comparaciones++;// Comparacion de primera iteracion del while inferior
            while (low <= high && list[low] <= pivot) {
                low++;
                comparaciones++;
            }

            // Search backward from right
            comparaciones++;// Comparacion de primera iteracion del while inferior
            while (low <= high && list[high] > pivot) {
                high--;
                comparaciones++;
            }
            // Swap two elements in the list
            comparaciones++;// Comparacion del if inferior
            if (high > low) {
                int temp = list[high];
                list[high] = list[low];
                list[low] = temp;
                intercambios++;
            }
        }

        comparaciones++;// Comparacion de primera iteracion del while inferior
        while (high > first && list[high] >= pivot) {
            high--;
            comparaciones++;
        }

        // Swap pivot with list[high]
        comparaciones++;// Comparacion del if inferior
        if (pivot > list[high]) {
            list[first] = list[high];
            list[high] = pivot;
            intercambios++;
            return new int[] { comparaciones, intercambios, high };
        } else {
            return new int[] { comparaciones, intercambios, first };
        }
    }

    /* function to sort arr using shellSort */
    public static int[] shellSort(int arr[]) {
        int n = arr.length;
        int comparaciones = 0;
        int intercambios = 0;

        // Start with a big gap, then reduce the gap
        for (int gap = n / 2; gap > 0; gap /= 2) {
            // Do a gapped insertion sort for this gap size.
            // The first gap elements a[0..gap-1] are already
            // in gapped order keep adding one more element
            // until the entire array is gap sorted
            for (int i = gap; i < n; i += 1) {
                // add a[i] to the elements that have been gap
                // sorted save a[i] in temp and make a hole at
                // position i
                int temp = arr[i];

                // shift earlier gap-sorted elements up until
                // the correct location for a[i] is found
                int j;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                    comparaciones++;// Comparacion en el condicional del for
                }

                // put temp (the original a[i]) in its correct
                // location
                arr[j] = temp;
                intercambios++;
            }
        }
        return new int[] { comparaciones, intercambios };
    }
}
