# Deber 16

12 de noviembre del 2020

## Instrucciones

**Utilizando el deber 12 se debe agregar los algoritmos de ordenamiento merge y quick**

**Utilizando el deber 15 se debe agregar el algoritmo de ordenamiento Shell**

Crear un programa para instrumentar los algoritmos de ordenamiento vistos en clase. Los algoritmo de ordenamiento están en los archivos adjuntos en este repositorio.

- Leer la cantidad de datos que iran en el arreglo a ordenar desde consola.
- Generar un arreglo con n elementos, los valores son generados de manera aleatorea.
- Leer la cantidad de veces que se debe correr la prueba desde consola.
- En cada algoritmo:
  - Contar el número de veces que se realiza la comparación.
  - Contar el número de veces que se realiza el intercambio de ordenamiento.
- Repetir los test de las veces que se paso en consola.
- Imprimir datos relevantes:
  - Promedio de veces que se realizó la comparación en cada algoritmo.
  - Promedio de veces que se realizó el intercambio en cada algoritmo.

### Nuevas instrucciones

- Se debe presentar la información en un formato de tabla.

## Notas

Notas de desarrollo: Ideas encontradas durante el desarrollo de la tarea.

- El arreglo debe clonarse para ser arregldo por cada algoritmo ya que la posición de los elemetos debe ser igual para todos los algoritmos.
- Se debe modificar los agoritmos originales obtenidos del libro para instrumentarlos.
- Si se require mas de una prueba entonces se debe recalcular los arreglos.
- Los métodos públicos en `AlgoritmosOrdenamiento` son los que deben ser llamados por el programa cliente.
- Los métodos privados en `AlgoritmosOrdenamiento` son métodos utilizados por métodos públicos y no deben ser llamados por ningún programa cliente.
- Los métodos devuelven un arreglo de dos posiciones con el formato:

```
a[0] //Conteo de comparaciones
a[1] //Conteo de intercambios
```
