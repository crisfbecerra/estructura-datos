import java.util.*;
import java.lang.reflect.*;

/**
 * @author Cristopher Becerra
 */

class deber16 {
    private static int upperLimit = 1000000;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Cantidad de elementos en el arreglo: ");
        int cantidadElementos = scan.nextInt();
        System.out.print("Repeticiones de las pruebas: ");
        int repeticionesPruebas = scan.nextInt();
        scan.close();

        // Generar el arreglo
        int[] a = null;

        // Recoger los metodos de la clase AlgortmosOrdenamiento, donde estan contenidos
        // todos los algoritmos de ordenamiento.
        // Facilita agregar más en un futuro
        Method[] algoritmos = filtarMetodosPublicos(AlgoritmosOrdenamiento.class.getDeclaredMethods());

        // List para contener los resultados de los diferentes arreglos
        // dentro un map com el nombre de cada metodo y un arreglo de 2 posiciones con
        // los datos
        // 0 - Conteo de comparaciones
        // 1 - Conteo de intercambios

        Map<String, Integer[][]> resultados = new HashMap<>();

        for (int i = 0; i < repeticionesPruebas; i++) {
            // Se genera nuevos datos para una nueva prueba
            a = generarArreglo(cantidadElementos);
            // Mapa para guardar los datos recopilados
            // int[][] datos = new int[cantidadElementos][2];
            for (Method m : algoritmos) {
                String methodName = m.getName();
                if (!resultados.containsKey(methodName)) {
                    // Si no contiene el metodo en el mapa lo agrega
                    resultados.put(methodName, new Integer[repeticionesPruebas][2]);
                }
                // System.out.println();
                // System.out.println("Testing " + methodName);
                try {
                    int[] r = (int[]) m.invoke(null, Arrays.copyOf(a, a.length));
                    Integer[][] v = resultados.get(methodName);
                    v[i][0] = r[0];// Comparaciones
                    v[i][1] = r[1];// Intercambios
                    // System.out.println("Comparaciones: " + r[0]);
                    // System.out.println("Intercambios: " + r[1]);
                } catch (Exception e) {
                    System.out.println("Error metodo " + methodName);
                    System.out.println(e.getMessage());
                }
                // System.out.println(Arrays.toString(a));
            }
        }

        System.out.println();
        // System.out.println("Resultados:");
        // for (Method m : algoritmos) {
        // String methodName = m.getName();
        // System.out.println();
        // System.out.println(methodName);
        // Integer[][] v = resultados.get(methodName);
        // int promedio = 0;
        // for (int i = 0; i < repeticionesPruebas; i++) {
        // promedio += v[i][0];
        // }
        // System.out.println("Promedio comparaciones: " + ((double) promedio /
        // repeticionesPruebas));

        // promedio = 0;
        // for (int i = 0; i < repeticionesPruebas; i++) {
        // promedio += v[i][1];
        // }
        // System.out.println("Promedio intercambios: " + ((double) promedio /
        // repeticionesPruebas));
        // }
        imprimirTablaDatos(resultados, repeticionesPruebas);
        System.out.println();
        System.out.println();
        imprimirTablaPromedios(resultados, repeticionesPruebas);
    }

    /**
     * Crear un nuevo arreglo de dimension n
     * 
     * @param int n
     * @return
     */
    public static int[] generarArreglo(int n) {
        int[] a = new int[n];
        for (int i = 0; i < a.length; i++) {
            a[i] = (int) (Math.random() * upperLimit);
        }
        return a;
    }

    private static Method[] filtarMetodosPublicos(Method[] list) {
        List<Method> retorno = new ArrayList<>();
        for (Method method : list) {
            if (Modifier.isPublic(method.getModifiers())) {
                // System.out.println(method.getName());
                retorno.add(method);
            }
        }
        return retorno.toArray(new Method[retorno.size()]);
    }

    private static void imprimirTablaDatos(Map<String, Integer[][]> datos, int numeroRepeticiones) {
        String anCoNo = "%13s";
        // Impresion header 1
        System.out.println("Comparaciones e intercambios totales");
        System.out.printf(anCoNo, "Algoritmo");
        for (int i = 0; i < numeroRepeticiones; i++) {
            System.out.print("|           " + ("Prueba " + i) + "           |");
        }
        System.out.println();
        // Impresion header 2
        System.out.printf(anCoNo, "----");
        for (int i = 0; i < numeroRepeticiones; i++) {
            System.out.print("| Comparaciones | Intercambios |");
        }
        System.out.println();
        for (Map.Entry<String, Integer[][]> d : datos.entrySet()) {
            System.out.printf(anCoNo, d.getKey());
            Integer[][] v = d.getValue();
            for (int i = 0; i < numeroRepeticiones; i++) {
                System.out.printf("|%14d |%13d |", v[i][0], v[i][1]);
            }
            System.out.println();
        }
    }

    private static void imprimirTablaPromedios(Map<String, Integer[][]> datos, int rep) {
        String anCoNo = "%13s";
        // Impresion header 1
        System.out.println("Promedio de compraciones e intercambios");
        System.out.printf(anCoNo, "Algoritmo");
        System.out.print("| Comparaciones | Intercambios |\n");
        for (Map.Entry<String, Integer[][]> d : datos.entrySet()) {
            System.out.printf(anCoNo, d.getKey());
            int intercambios = 0;
            int comparaciones = 0;
            Integer[][] v = d.getValue();
            for (int i = 0; i < rep; i++) {
                comparaciones += v[i][0];
            }
            for (int i = 0; i < rep; i++) {
                intercambios += v[i][1];
            }
            System.out.printf("|%14.3f |%13.3f |", ((double) comparaciones / rep), ((double) intercambios / rep));
            System.out.println();
        }
    }
}