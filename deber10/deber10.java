import java.util.Scanner;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class deber10 {
    private static Registro[] registros;

    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.print("Cantidad de pruebas: ");
        int cantidadPruebas = input.nextInt();
        input.close();
        if (cantidadPruebas < 1) {
            System.out.println("La cantidad de pruebas debe ser mayor a cero");
            System.exit(0);
        }
        registros = new Registro[cantidadPruebas];
        for (int i = 0; i < cantidadPruebas; i++) {
            int valor1 = (int) (Math.random() * 100000000);
            int valor2 = (int) (Math.random() * 100000000);
            registros[i] = new Registro(valor1, valor2);
            int[] resultados = gcd(valor1, valor2);
            registros[i].setMcd(resultados[0]);
            registros[i].setNumeroIteraciones(resultados[1]);
        }
        Arrays.sort(registros);
        System.out.println("Resultados:");
        System.out.println("[Valor 1, Valor 2, MCD, Iteraciones]");
        System.out.println(Arrays.toString(registros));
        System.out.println();
        printMinimos();
        System.out.println();
        printMaximos();
        System.out.println();
        printPromedio();
        System.out.println();
        printMediana();
        System.out.println();
        printModa();

    }

    /**
     * 
     * @param int m
     * @param int n
     * @return void
     */
    public static int[] gcd(int m, int n) {
        int r = m % n;
        int iteraciones = 1;
        while (r != 0) {
            m = n;
            n = r;
            r = m % n;
            iteraciones++;
        }
        // return n;
        return new int[] { n, iteraciones };

    }

    /**
     * Imprimir los mínimos encontrado en registros
     */
    public static void printMinimos() {

        // Busqueda del minimo
        Registro minimo = registros[0];
        for (int i = 1; i < registros.length; i++) {
            if (minimo.getNumeroIteraciones() > registros[i].getNumeroIteraciones())
                minimo = registros[i];
        }

        // Impresion de todos los minimos
        System.out.println("Minimos:");
        System.out.println("[Valor 1, Valor 2, MCD, Iteraciones]");
        for (Registro r : registros) {
            if (minimo.getNumeroIteraciones() == r.getNumeroIteraciones())
                System.out.print(r);
        }
    }

    /**
     * Impresion de maximos de los resultados
     */
    public static void printMaximos() {
        // Busqueda del máximo
        Registro max = registros[0];
        for (int i = 0; i < registros.length; i++) {
            if (max.getNumeroIteraciones() < registros[i].getNumeroIteraciones())
                max = registros[i];
        }

        // Impresion de registros
        System.out.println("Máximos:");
        System.out.println("[Valor 1, Valor 2, MCD, Iteraciones]");
        for (Registro r : registros) {
            if (max.getNumeroIteraciones() == r.getNumeroIteraciones())
                System.out.print(r);
        }
    }

    /**
     * Impresion de promedio de iteraciones
     */
    public static void printPromedio() {

        int suma = 0;
        for (Registro r : registros) {
            suma += r.getNumeroIteraciones();
        }
        System.out.println("Promedio: " + (double) suma / (double) registros.length);
    }

    /**
     * Impresion de mediana de la itraciones
     */
    public static void printMediana() {
        // El arreglo ya esta ordenado. Se busca el punto medio
        System.out.println("Mediana: " + registros[(registros.length / 2) - 1]);
    }

    /**
     * Impresion de la moda
     */
    public static void printModa() {
        Map<Integer, Integer> m = new HashMap<>();
        for (Registro r : registros) {
            if (m.containsKey(r.getNumeroIteraciones())) {
                m.put(r.getNumeroIteraciones(), m.get(r.getNumeroIteraciones()) + 1);
            } else {
                m.put(r.getNumeroIteraciones(), 1);
            }
        }
        System.out.println(m);

        // Busqueda del mayor
        int max = 0;
        for (Map.Entry<Integer, Integer> entry : m.entrySet()) {
            if (max < entry.getValue())
                max = entry.getValue();
        }

        // Impresion de las mayores modas
        System.out.println("Moda:");
        System.out.println("[Cantidad iteraciones : Repeticiones]");
        for (Map.Entry<Integer, Integer> entry : m.entrySet()) {
            if (max == entry.getValue())
                System.out.println("[" + entry.getKey() + " : " + entry.getValue() + "]");
        }
    }

}

class Registro implements Comparable<Registro> {
    private int valor1;
    private int valor2;
    private int mcd;
    private int numeroIteraciones;

    public Registro(int valor1, int valor2) {
        this.valor1 = valor1;
        this.valor2 = valor2;
    }

    int getValor1() {
        return valor1;
    }

    int getValor2() {
        return valor2;
    }

    int getMcd() {
        return mcd;
    }

    int getNumeroIteraciones() {
        return numeroIteraciones;
    }

    void setMcd(int mcd) throws Exception {
        if (mcd < 0) {
            throw new Exception("Mcd debe ser mayor de 0");
        }

        this.mcd = mcd;
    }

    void setNumeroIteraciones(int ite) throws Exception {
        if (ite < 1) {
            throw new Exception("Numero de iteraciones debe ser mayor de 1");
        }
        this.numeroIteraciones = ite;
    }

    @Override
    public String toString() {
        return "[" + valor1 + "," + valor2 + "," + mcd + "," + numeroIteraciones + "]\n";
    }

    @Override
    public int compareTo(Registro r) {
        if (numeroIteraciones > r.getNumeroIteraciones())
            return 10;
        if (numeroIteraciones < r.getNumeroIteraciones()) {
            return -10;
        }
        return 0;
    }
}