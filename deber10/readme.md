# Deber 10

19 de Octubre del 2020

## Instrumentación de un código

La instrumentión de un código se refiere a la inserción de código necesario para medir la eficiencia de un algoritmo ya escrito. Este puede ser la inclusion de un contador dentro de los bucles, almacenar los resultados de una función para un posterior análisis.

## Instrucciones

Crear un programa para el cálculo de Maximo Común Divisor de dos números enteros al azar utilizando el algoritmo de Euclides en formato de iteraciones (no en formato de recursividad). Calcular la cantidad de iteraciones necesarias para cada cálculo de Máximo Común Divisor.

- Leer desde consola la cantidad de MCD que se van a calcular.
- Para cada cálculo de MCD es necesario generar dos números al azar entre 1 y 100.000.000.
- Al finalizar los cálculos se debe presentar:
  - Mínimo. Con los valores a calcular, el MCD resultante y el número de iteraciones.
  - Máximo. Con los valores a calcular, el MCD resultante y el número de iteraciones.
  - Promedio de iteraciones necesarias.
  - Media de las iteraciones necesarias.
  - Moda de las iteraciones necesarias.
