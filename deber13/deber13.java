import java.lang.Math;
import java.util.Arrays;

class deber13 {
    final static int MAX_VALUE = 200;
    final static int MAX_REP = 5;
    final static int ARRAY_SIZE = 100;

    public static void main(String[] args) {
        int[] a = generarArreglo();
        System.out.println("Arreglo original");
        System.out.println(Arrays.toString(a));
        insertionSort(a);
        System.out.println();
        System.out.println("Arreglo ordenado");
        System.out.println(Arrays.toString(a));
        deleteDuplicates(a);
        System.out.println();
        System.out.println("Arreglo eliminado duplicados");
        System.out.println(Arrays.toString(a));

    }

    /**
     * Genera un arreglo con numeros random y numeros repetidos
     * 
     * @return int[]
     */
    public static int[] generarArreglo() {
        int[] retorno = new int[ARRAY_SIZE];
        int pos = 0;
        while (pos < retorno.length) {
            int value = (int) ((Math.random() * (MAX_VALUE)) + 1);
            int rep = (int) ((Math.random() * (MAX_REP)) + 1);
            for (int i = 0; i < rep && pos < retorno.length; i++, pos++) {
                retorno[pos] = value;
            }
        }
        return retorno;
    }

    /**
     * Ordena un arreglo con el método Insertion Sort
     * 
     * @param list
     */
    public static void insertionSort(int[] list) {
        for (int i = 1; i < list.length; i++) {
            /**
             * insert list[i] into a sorted sublist list[0..i-1] so that list[0..i] is
             * sorted.
             */
            int currentElement = list[i];
            int k;
            for (k = i - 1; k >= 0 && list[k] > currentElement; k--) {
                list[k + 1] = list[k];
            }

            // Insert the current element into list[k+1]
            list[k + 1] = currentElement;
        }
    }

    public static void deleteDuplicates(int[] a) {
        int i = 0;
        int j = 0;
        while (i < a.length) {
            a[j] = a[i];
            do {
                i++;
            } while ((i < a.length) && (a[i] == a[j]));
            j++;
        }
        for (int k = j; k < a.length; k++) {
            a[k] = -1;
        }
    }
}